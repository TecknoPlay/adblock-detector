# AdBlock Detector #

## Table des matieres ##

* Resumé
* Configuration
* Changelog
* To Do

## Resumé ##
AdBlock Detector est un ensemble de 2 scripts ( un JS et un PHP) servant à détecter la présence de adBlock.

Si adBlock est détecté, le visiteur en sera notifié par une alerte en haut de sa page. 

Chaque visiteur est loggé (adresse IP) afin de voir l'utilité du script et le pourcentage de visiteur avec adBlock. 

Chaque activation ( ou désactivation ) de adBlock modifiera l'enregistrement du visiteur dans la BDD. 

## Configuration

### Minimum requis ###
* jQuery
* ( une base de donnée ) : le script peut fonctionner sans, mais il n'y aura aucun log

### Installer ADBD ###
Lier les 3 fichiers suivant sur les page ou vous desirez ADBD : advertisement.js, anti_adb.css et anti_adb.js

```
#!html

<script type="text/javascript" src="advertisement.js"></script>
<link rel="stylesheet" type="text/css" href="anti_adb.css" />
<script src="anti_adb.js"></script>
```


### anti_adb.JS ###
var titre : Premiere ligne de l'alerte
var sous_titre : petit texte sous le titre indiquant les raison de l'utilisation de ADBD 

### antiadb.php ###
PDO_IP : IP de votre MySQL
PDO_DB : nom de la base de donnee 
PDO_USER : username MySQL 
PDO_PASS : mot de passe MySQL